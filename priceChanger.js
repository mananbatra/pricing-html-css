document.addEventListener('DOMContentLoaded', function() {
        const checkbox = document.getElementById('toggle');
        const basic_price = document.getElementById('1');
        const pro_price = document.getElementById('2');
        const master_price = document.getElementById('3');
        const lable1 = document.getElementById('l1');
        const lable2 = document.getElementById('l2');
        
      
        checkbox.addEventListener('change', function() {
          if (checkbox.checked) {
            basic_price.textContent = `19.99`;
            pro_price.textContent = '24.99';
            master_price.textContent = '39.99';
            lable2.classList.add('changeColor');
            lable1.classList.remove('changeColor');
          } else {
            basic_price.textContent = '199.99';
            pro_price.textContent = '249.99';
            master_price.textContent = '399.99';
            lable2.classList.remove('changeColor');
            lable1.classList.add('changeColor');
          }
        });
      });